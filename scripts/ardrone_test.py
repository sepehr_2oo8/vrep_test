#!/usr/bin/env python

from ardrone_autonomy.srv import LedAnim
import rospy


def main():
    rospy.wait_for_service('ardrone/setledanimation')
    try:
        led_srv = rospy.ServiceProxy('ardrone/setledanimation',LedAnim)

        return led_srv(2,1,20)
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

if __name__ == "__main__":
    main()