#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from vrep_test.msg import t1
from ros_control.ControlSystem import ControlSystem
from geometry_msgs.msg import Point
import cv2
import signal
import sys
import time
import ardrone_autonomy.srv as ardrone_srv
from sensor_msgs.msg import Joy

led_srv = rospy.ServiceProxy('ardrone/setledanimation',ardrone_srv.LedAnim)
Input = {'vel_X' : 0,'vel_Y' : 0,'vel_Z' : 0,'vel_Yaw' : 0}

def signal_handler(signal, frame):
	print "shutting down!"
	cv2.destroyAllWindows()
	sys.exit(0)
def joyStickCallback(data):
	translation_step = 0.1
	Input = {
		'vel_X' : data.axes[0]*translation_step, 
		'vel_Y' : data.axes[1]*translation_step, 
		'vel_Z' : (data.buttons[5]-data.buttons[7])*translation_step, 
		'vel_Yaw' : (data.buttons[4]-data.buttons[6])*translation_step
	}
	
	if data.axes[0] == 1:
		led_srv(11,4,1)
	if data.axes[0] == -1:
		led_srv(12,4,1)
	if data.axes[1] == 1:
		led_srv(2,4,1)
	if data.axes[1] == -1:
		led_srv(3,4,1)
	if data.buttons[0] == 1:
		led_srv(8,4,3)
	if data.buttons[1] == 1:
		led_srv(9,4,3)


def main():
	rospy.init_node('moduleTest')

	rospy.loginfo ("Joystick Subscriber has established")
	rospy.Subscriber('joy',Joy,joyStickCallback)

	CS = ControlSystem()

	signal.signal(signal.SIGINT, signal_handler)
	i = 0
	while True:
		try:
			i += 1
			print "---------------- " , i
			CS.run(Input)
			rospy.sleep(1.0)
  		except KeyboardInterrupt:
  			print "Shutting down"
  			cv2.destroyAllWindows()
  			break


if __name__ == '__main__':
	main()
