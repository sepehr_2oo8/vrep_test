#!/usr/bin/env python

import rospy

from sensor_msgs.msg import Joy
from geometry_msgs.msg import Point

class quadControler:
	quadType = 'not_init'
	def __init__(self,sim_or_real):
		self.pub = rospy.Publisher ('myRosBubbleRob/quad_target',Point,queue_size = 1)
		self.quadType = sim_or_real

def joyStickCallback(data):
	rospy.loginfo("joyStickCallback")
	translation_step = 0.01
	p = Point (data.axes[0]*translation_step,data.axes[1]*translation_step,(data.buttons[5]-data.buttons[7])*translation_step)
	QC.pub.publish(p)

def joyStickControl():
	rospy.init_node('joyStickControl')
	rospy.loginfo ("Publisher has established")
	rospy.Subscriber('joy',Joy,joyStickCallback)
	rospy.spin()


QC = quadControler('sim')
if __name__ == '__main__':
	joyStickControl()
