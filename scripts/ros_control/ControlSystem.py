import rospy
from Observer import Observer
from Feedback import Feedback
from Transmitter import Transmitter
from Controller import Controller


class ControlSystem (object):
    def __init__(self, name = "AnymControlClass", control_system_type = 'ardrone_manual', has_observer=True, is_linear=True):
        self.control_system_type = control_system_type
        self.name = name

        print "control system initialization: \n"
        self.observer = Observer(control_system_type)
        self.feedback = Feedback(control_system_type)
        self.transmitter = Transmitter(control_system_type)
        self.controller = Controller(control_system_type)

        


    def run(self,Input):
        
        self.observer.run()
        self.controller.run(Input)
        self.transmitter.run()
        self.feedback.run()
        
    
    def __str__(self):
        print self.name + "it's done"