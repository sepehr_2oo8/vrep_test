import rospy
import numpy as np
from std_msgs.msg import String
import ast
import ardrone_autonomy.srv as ardrone_srv
class Controller(object):
    def __init__(self, controller_type = '', controller_in = '', controller_out = ''):
        # General Configuration
        self.observer_out_sub = rospy.Subscriber("ros_control/observer_out", String, self.updateObserverOut)
        self.control_out_pub = rospy.Publisher("ros_control/control_out", String,queue_size = 1)

        if controller_type == 'Quadricopter':
            self.observer_in = {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.input= {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.output = {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.controller_imp_type = 'Quadricopter_PID'
            print "\n -------Controller Configuration -------"
            print "controller inputs are : " + str(self.input)
            print "controller outputs are : " + str (self.output)
            self.total_error = np.zeros(len(self.input))
            self.prev_error = np.zeros(len(self.input))
            return
        
        if controller_type == 'ardrone_manual':
            self.input = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.output = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.controller_imp_type = 'ardrone_manual'
            
        if controller_type == 'ardrone_auto':
            self.data_dict = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.output = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.controller_imp_type = 'ardrone_auto'

        self.input = controller_in
        self.output = controller_out
    
    def updateObserverOut(self,data):
        self.observer_in = ast.literal_eval(data.data)
        print "observer input to controller = ",self.observer_in

    def run(self,Input):
        if self.controller_imp_type == 'Quadricopter_PID':
            for w in Input:
                try:
                    self.input[w]
                    self.observer_in[w]
                except:
                    rospy.loginfo("there is no: " + w +"as feedback or input" )
            error = np.zeros(len(self.input))
            #print "controller input is:"
            #print Input
            for i in range(len(Input)):
                error[i] = Input.values()[i] + self.observer_in[Input.keys()[i]]

            rospy.loginfo(error)
            P = 0.001
            I = 0
            D = 0

            out = error*P + self.total_error*I + (self.prev_error - error)*D
            for i in range(len(out)):
                self.output[self.output.keys()[i]] = out[i]

            self.total_error += error
            self.prev_error = error

            msg_out = str(self.output)
            self.control_out_pub.publish(msg_out)
        
        if self.controller_imp_type == 'ardrone_manual':

            msg_out = str(Input)
            self.control_out_pub.publish(msg_out)
