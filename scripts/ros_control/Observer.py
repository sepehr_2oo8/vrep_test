import rospy
import numpy as np
import math
import time
from std_msgs.msg import String
import ast


class Observer(object):
    def __init__(self,observer_type = 'Quadricopter', feedback_in = '', control_in = '', observer_state = ''):
        #General Configuration
        self.observer_type = observer_type
        self.control_out_sub = rospy.Subscriber("ros_control/control_out",String,self.updateControlOut)
        self.feedback_sub = rospy.Subscriber("ros_control/feedback", String, self.updateFeedbackOut)
        self.observer_out_pub = rospy.Publisher("ros_control/observer_out", String, queue_size = 10)
        self.feedback_vector_updated = 0
        if observer_type == 'Quadricopter':
            self.feedback_in = {
                'qX' : 0,
                'qY' : 0,
                'qZ' : 0,
                'aX' : 0,
                'aY' : 0,
                'aZ' : 0
            }
            self.control_in = {
                'roll' : 0,
                'pitch' : 0,
                'yaw' : 0,
                'up' : 0
            }
            self.observer_state = {
                'Phi' : 0,
                'Theta' : 0,
                'Psi' : 0,
                'vX' : 0,
                'vY' : 0,
                'vZ' : 0,
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.X = np.zeros(len(self.observer_state))
            self.t = time.time()
        if (observer_type == 'ardrone_manual') | (observer_type == 'ardrone_auto'):
            self.feedback_in = {
                "rotX" : 0,
                "rotY" : 0,
                "rotZ" : 0,
                "magX" : 0,
                "magY" : 0,
                "magZ" : 0,
                "aX" : 0,
                "aY" : 0,
                "aZ" : 0,
                "altd" : 0,
                "vX" : 0,
                "vY" : 0,
                "vZ" : 0,
                "battery" : 0,
                "pressure" : 0
                # There is more
            }

            self.control_in = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.observer_state = {
                'Phi' : 0,
                'Theta' : 0,
                'Psi' : 0,
                'vX' : 0,
                'vY' : 0,
                'vZ' : 0,
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
        else:
            self.control_in = control_in
            self.observer_state = observer_state
            self.feedback_in = feedback_in

        print "\n---------- Observer Configuration---------"
        print "control input" + str(self.control_in)
        print "feedback input" + str(self.feedback_in)
        print "observer input" + str(self.observer_state)



    def updateControlOut(self,data):
        self.control_in = ast.literal_eval(data.data)
        print self.control_in
    def updateFeedbackOut(self,data):
        if self.observer_type == 1:
            if self.feedback_vector_updated == False:
                self.X[np.array(range(6))] = 0
            self.feedback_vector_updated = 1
        
        self.feedback_in = ast.literal_eval(data.data)
        rospy.loginfo( self.feedback_in)

    def run(self):

        if self.observer_type == 'Quadricopter':
            # observer module comes here
            # it's possible to use ros topic, wrapping a c++ code or direct coding here
            U = np.zeros(6)
            U[0] = self.feedback_in['qX']
            U[1] = self.feedback_in['qY']
            U[2] = self.feedback_in['qZ']
            U[3] = self.feedback_in['aX']
            U[4] = self.feedback_in['aY']
            U[5] = self.feedback_in['aZ']

            #print "U = " , U
            dX = np.zeros(len(self.X))
            g = 9.81
            m=0.2
            mu=0.57
            T=m*g/math.cos(self.X[0])/math.cos(self.X[1])
            dX[0] = U[0]+U[1]*(math.sin(self.X[0])*math.tan(self.X[1]))+U[2]*(math.cos(self.X[0])*math.tan(self.X[1]))
            dX[1] = U[1]*math.cos(self.X[0])-U[2]*math.sin(self.X[0])
            dX[2] = U[1]*math.sin(self.X[0])/math.cos(self.X[1])+U[2]*math.cos(self.X[0])/math.cos(self.X[1])
            dX[3] = -g*math.sin(self.X[1])-mu/m*self.X[3]
            dX[4] = -g*math.cos(self.X[1])*math.sin(self.X[0])-mu/m*self.X[4]
            #dX[5] = g*math.cos(self.X[0])  *math.cos(self.X[1])-T/m+self.X[8]/m
            dX[5] = U[5]+g
            dX[6] = self.X[3]
            dX[7] = self.X[4]
            dX[8] = self.X[5]

            self.X += dX * (time.time() - self.t)
            self.t = time.time()
            
            self.observer_state['Phi'] = self.X[0]
            self.observer_state['Theta'] = self.X[1]
            self.observer_state['Psi'] = self.X[2]
            self.observer_state['vX'] = self.X[3]
            self.observer_state['vY'] = self.X[4]
            self.observer_state['vZ'] = self.X[5]
            self.observer_state['X'] = self.X[6]
            self.observer_state['Y'] = self.X[7]
            self.observer_state['Z'] = self.X[8]

            msg_out = str(self.observer_state)
            self.observer_out_pub.publish(msg_out)



