import rospy
from geometry_msgs.msg import Point
from std_msgs.msg import String
import ast
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

class Transmitter(object):
    def __init__(self,transmitter_type = '', transmitter_in = '', transmitter_out = ''):
        # General Configuration
        self.control_out_sub = rospy.Subscriber("ros_control/control_out", String, self.updateControlOut)
        
        if transmitter_type == 'Quadricopter':
            self.data_dict = {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            print "\n------ Transmitter Configuration --------"
            print "transmitter data outputs are : " + str(self.data_dict)
            self.out_publisher = rospy.Publisher ('myRosBubbleRob/quad_target',Point,queue_size = 1)
            return

        if (transmitter_type == "ardrone_manual") | (transmitter_type == "ardrone_auto"):
            
            self.data_dict = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.out_publisher = rospy.Publisher ('cmd_vel',Twist,queue_size = 1)
            print "\n------ Transmitter Configuration --------"
            print "transmitter data outputs are : " + str(self.data_dict)
            return   

        self.input = transmitter_in
        self.output = transmitter_out

    def updateControlOut(self,data):
        self.data_dict = ast.literal_eval(data.data)
        print self.data_dict

    def run(self,receiver_type = 'Quadricopter' ):
        if receiver_type == 'Quadricopter':
            print 'values has transmitted to Quadricopter'
            return

        if (receiver_type == 'ardrone_auto') | (receiver_type == 'ardrone_manual'):
            
            linear = Vector3(self.data_dict[vel_X],self.data_dict[vel_Y],self.data_dict[vel_Z])
            angular = Vector3(0,0,self.data_dict[vel_Yaw])
            self.out_publisher.publish(linear,angular)
            print 'values has transmitted to ardrone'
            return 
        print "there is no method available for " + receiver_type
        return



